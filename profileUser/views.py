from django.shortcuts import render
from django.views.generic import DetailView, UpdateView
from .models import Profile
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.http import HttpResponseForbidden
# Create your views here.
from django.shortcuts import redirect
from django import forms
from django.forms import DateInput
from django.contrib.auth.models import User
# Вся сметана
# 2 столовых ложки пасты
# полчайно соли
# поперчить немного(специи)
#
#
# 150
# 30


class ProfileForm(forms.ModelForm):
    birthday = forms.DateField(widget=forms.TextInput(attrs=
    {
        'type': 'date'
    }))

    class Meta:
        model = Profile
        fields = (
          "gender",
          "birthday",
          "status_relationship",
          "status_relationship_user",
          "city",
          "status_profile",
          "mobile",
          "nickname",
            "last_name",
          "email",
            "first_name"
        )



    def save(self, *args, **kwargs):
        return super(ProfileForm, self).save(*args, **kwargs)


class ProfileView(DetailView):
    model = Profile


    def get_object(self, queryset=None):
        return get_object_or_404(
            Profile,
            nickname=self.kwargs['slug'],
        )


class ProfileUpdate(UpdateView):
    form_class = ProfileForm

    def get_object(self, queryset=None):
        a = get_object_or_404(
            Profile,
            nickname=self.kwargs['slug'],
        ).user.id
        if  a!= self.request.user.id:
            raise Http404
        else:
            return get_object_or_404(
                Profile,
                nickname=self.kwargs['slug'],
            )


def redirect_view(request):
    if request.user.username=='':
        return redirect('google.com')
    else:
        return redirect('/'+get_object_or_404(
                Profile,
                user=request.user.id,
            ).nickname)