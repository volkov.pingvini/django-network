# Generated by Django 2.0.5 on 2018-06-23 15:54

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('profileUser', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='status_relationship_user',
        ),
        migrations.AddField(
            model_name='profile',
            name='status_relationship_user',
            field=models.ManyToManyField(default=None, related_name='user_relationship', to=settings.AUTH_USER_MODEL),
        ),
    ]
