# Generated by Django 2.0.5 on 2018-06-23 16:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('profileUser', '0006_auto_20180623_2302'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='status_relationship_user',
        ),
        migrations.AddField(
            model_name='profile',
            name='status_relationship_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user_relationship', to=settings.AUTH_USER_MODEL),
        ),
    ]
