from django.db import models
from django.contrib.auth.models import User


class Relationship(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Profile(models.Model):
    class Meta:
        unique_together = ('nickname',)

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gender = models.BooleanField(default=True)
    last_name = models.CharField(max_length=30, default='')
    first_name = models.CharField(max_length=30, default='')
    birthday = models.DateField()
    status_relationship = models.ForeignKey(Relationship, null=True, on_delete=models.SET_NULL)
    status_relationship_user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL,
                                                    related_name='user_relationship')
    city = models.CharField(max_length=300)
    status_profile = models.CharField(max_length=300)
    mobile = models.CharField(max_length=11)

    nickname = models.CharField(max_length=100)

    def get_absolute_url(self):
        return "/%s" % self.nickname

    def __str__(self):
        return self.nickname
