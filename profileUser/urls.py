from django.urls import path
from .views import ProfileView, redirect_view, ProfileUpdate

urlpatterns = [
    path('<slug:slug>/edit', ProfileUpdate.as_view()),
    path('<slug:slug>', ProfileView.as_view()),
    path('', redirect_view)
]
